import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useStocksStore = defineStore('stocks', () => {
  const portfolios = ref([]);
  const selectedPortfolioName = ref(null);

  // const apiURL = "/api/stocks.json";
  const firebaseURL = "https://udemy-vue-training-default-rtdb.firebaseio.com/"

  const portfolioNames = computed( ()=> {
    let names = []
    for (const portfolio of portfolios.value ) {
      names.push(portfolio.name);
    }
    return names
  });

  const selectedPortfolio = computed(() => {
    return portfolios.value.find((obj) => obj.name === selectedPortfolioName.value )
  });

  async function fetchPortfolios() {
    const requestURL = firebaseURL + 'stocks/portfolios.json';
    const response = await fetch(requestURL, {method: 'GET',});
    const responseData = await response.json();

    if (!response.ok) {
      console.log("error:", responseData.message);
      const error = new Error(responseData.message || "Failed to fetch portfolios!");
      throw error;
    }
    portfolios.value = [];
    for (const portfolioID in responseData) {
      portfolios.value.push(responseData[portfolioID]);
    }

    // set initial selectedPortfolioName
    if (portfolioNames.value.length > 0 && selectedPortfolioName.value === null) {
      selectedPortfolioName.value = portfolioNames.value[0];
    }
  }

  async function addNewPortfolio(portfolioName)  {
    const requestURL = firebaseURL + 'stocks/portfolios/' + portfolioName + '.json';

    if (!portfolioName || portfolioName === '' || portfolioName.match(/^\s*$/)) {
      throw new Error("portfolio name can not be blank");
    }
    if (portfolioNames.value.includes(portfolioName)) {
      throw new Error("portfolio name already exists!");
    }
    const response = await fetch(requestURL, {
      method: 'PUT',
      body: JSON.stringify({name: portfolioName, stocks:[]}),
    });

    const responseData = await response.json();

    if (!response.ok) {
      console.log("error: ", responseData.message)
      const error = new Error(responseData.message || "Failed to Send Request!");
      throw error;
    }

    const newPortfolio = {name: portfolioName, stocks: []};
    portfolios.value.push(newPortfolio);

  }

  async function deletePortfolio (portfolioName) {
    const requestURL = firebaseURL + 'stocks/portfolios/' + portfolioName + '.json';
    const response = await fetch(requestURL, {
      method: 'DELETE',
    });

    const responseData = await response.json();

    if (!response.ok) {
      console.log("error: ", responseData.message)
      const error = new Error(responseData.message || "Failed to Send Request!");
      throw error;
    }
    
    const filteredPortfolios = portfolios.value.filter(portfolio => portfolio.name != portfolioName);
    portfolios.value = filteredPortfolios;
  }

  async function addStock(payload) {
    const portfolioName = payload.portfolioName;
    const stockName = payload.stockName;

    if (!portfolioName || portfolioName === '' || portfolioName.match(/^\s*$/)) {
      throw new Error("portfolio name can not be blank");
    }
    if (!stockName || portfolioName === '' || stockName.match(/^\s*$/) ) {
      throw new Error("stock name can not be blank");
    }

    const portfolio = portfolios.value.find( obj => obj.name === portfolioName);

    if (!portfolio) {
      throw new Error("portfolio ", portfolioName ,"not found in portfolios");
    }

    let stocks = []

    if (portfolio.stocks ) {
      stocks = portfolio.stocks;;
    }

    if (stocks.includes(stockName)) {
      throw new Error("portfolio already has the stock");
    }

    stocks.push(stockName);

    const requestData = {stocks: stocks};
    const requestURL = firebaseURL + 'stocks/portfolios/' + portfolioName + '.json';
    const response = await fetch(requestURL, {
      method: 'PATCH',
      body: JSON.stringify(requestData),
    });
    const responseData = await response.json();

    if (!response.ok) {
      console.log("error:", responseData.message)
      const error = new Error(responseData.message || "Failed to add stock!");
      throw error;
    }

    const index = portfolios.value.findIndex( obj => obj.name === portfolioName);

    if (!portfolios.value[index].stocks.includes(stockName)) {
      portfolios.value[index].stocks.push(stockName);
    }
  }

  async function deleteStock(payload) {
    const portfolioName = payload.portfolioName;
    const stockName = payload.stockName;

    const portfolio = portfolios.value.find( obj => obj.name === portfolioName);

    if (!portfolio) {
      throw new Error("portfolio ", portfolioName ,"not found in portfolios");
    }

    let stocks = []

    if (portfolio.stocks ) {
      stocks = portfolio.stocks;;
    }

    if (stocks && !stocks.includes(stockName)) {
      throw new Error("portfolio does not have this stock");
    }

    const filteredStocks = stocks.filter(item => item != stockName);
    const requestData = {stocks: filteredStocks};
    const requestURL = firebaseURL + 'stocks/portfolios/' + portfolioName + '.json';
    const response = await fetch(requestURL, {
      method: 'PATCH',
      body: JSON.stringify(requestData),
    });
    const responseData = await response.json();

    if (!response.ok) {
      console.log("error:", responseData.message)
      const error = new Error(responseData.message || "Failed to delete stock!");
      throw error;
    }
  
    const index = portfolios.value.findIndex( obj => obj.name === portfolioName);
    portfolios.value[index].stocks = filteredStocks;
  }

  return {
    portfolios,
    portfolioNames, 
    selectedPortfolioName, 
    selectedPortfolio, 
    fetchPortfolios,
    addNewPortfolio,
    deletePortfolio,
    addStock,
    deleteStock
  }
})
