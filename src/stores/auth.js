import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth', () => {
  const user = ref({
    userName: null,
    userID: null,
    token: null,
    tokenExpiration: null

  });

  let timer;
  const webAPIKey = 'AIzaSyDeXvtPqh6K9uVA814P92fuqtuwqDC0dgU';
  const loginURL = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" + webAPIKey;

  const isAuthenticated = computed( () => {
    return !!user.value.token;
  })

  async function userLogin(payload) {
    /*
    Property Name	Type	Description
    idToken	string	A Firebase Auth ID token for the newly created user.
    email	string	The email for the newly created user.
    refreshToken	string	A Firebase Auth refresh token for the newly created user.
    expiresIn	string	The number of seconds in which the ID token expires.
    localId	string	The uid of the newly created user.
    */
    const response = await fetch(loginURL, {
      method: 'POST',
      body: JSON.stringify({
        email: payload.email,
        password: payload.password,
        returnSecureToken: true
      }),
    });
    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(responseData.message || responseData.error.message ||  "Failed to login");
      throw error;
    }

    //default is 3600 seconds, ie, 1 hour; below will convert it to mili seconds
    // a '+' sign in front of the var converts it to number
    const expiresIn = +responseData.expiresIn * 1000;
    // const expiresIn = 5000;
    const expirationDate = new Date().getTime() + expiresIn;

    localStorage.setItem('token', responseData.idToken);
    localStorage.setItem('userID', responseData.localId);
    localStorage.setItem('userName', payload.email);
    localStorage.setItem('tokenExpiration', expirationDate);

    user.value = {
      token: responseData.idToken,
      userID: responseData.localId,
      userName: payload.email,
      tokenExpiration: expirationDate
    }

    timer = setTimeout(function(){
      userLogout();
    }, expiresIn);
  }

  function userLogout () {
    localStorage.removeItem('token');
    localStorage.removeItem('userID');
    localStorage.removeItem('userName');
    localStorage.removeItem('tokenExpiration');

    clearTimeout(timer);

    user.value = {
      token: null,
      userID: null,
      userName: null,
      tokenExpiration: null
    }
  }

  function userAutoLogin() {
    const token = localStorage.getItem('token');
    const userID = localStorage.getItem('userID');
    const userName = localStorage.getItem('userName');
    const tokenExpiration = localStorage.getItem('tokenExpiration');

    const expiresIn = +tokenExpiration - new Date().getTime();

    if (expiresIn < 0 ) {
      return;
    }

    timer = setTimeout(function() {
      userLogout();
    }, expiresIn);

    if (token && userID) {
      user.value = {
        token: token,
        userID: userID,
        userName: userName,
        tokenExpiration: tokenExpiration
      }
    }
  }

  return { user, isAuthenticated, userLogin, userLogout, userAutoLogin }
})
